/**
 * Copyright (c) 2015 Pablo Andrés Díaz
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the MIT/X11 license. The text of the
 * license can be found at http://www.opensource.org/licenses/mit-license.php and copy of the license can be found on the project
 * website http://www.extensiblecatalog.org/.
 *
 */

package org.extensiblecatalog.ncip.v2.siabuc.util;

import org.extensiblecatalog.ncip.v2.common.DefaultConnectorConfiguration;
import org.extensiblecatalog.ncip.v2.service.ServiceException;
import java.util.Properties;


public class ILSConfiguration extends DefaultConnectorConfiguration {

    public ILSConfiguration() throws ServiceException {
    	super(new Properties());
    }
    
    public ILSConfiguration(Properties properties) throws ServiceException {
    	super(properties);
    }
    
}
