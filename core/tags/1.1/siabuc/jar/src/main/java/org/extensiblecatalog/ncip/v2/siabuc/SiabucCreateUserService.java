/**
 * Copyright (c) 2010 eXtensible Catalog Organization
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the MIT/X11 license. The text of the license can be
 * found at http://www.opensource.org/licenses/mit-license.php.
 */

package org.extensiblecatalog.ncip.v2.siabuc;


import java.util.ArrayList;
import java.util.List;


import org.apache.log4j.Logger;
import org.extensiblecatalog.ncip.v2.service.*;
import org.extensiblecatalog.ncip.v2.siabuc.util.*;

/**
 * This class implements Lookup Item service for siabuc9 connector
 * @author pabloadi
 *
 */
public class SiabucCreateUserService implements CreateUserService {
	
	static Logger log = Logger.getLogger(SiabucCreateUserService.class);
	SiabucRemoteServiceManager siabucSvcMgr;
	
	@Override
	public CreateUserResponseData performService(
			CreateUserInitiationData initData,
			ServiceContext serviceContext, RemoteServiceManager serviceManager)
			throws ServiceException {
		// TODO Auto-generated method stub
		
		siabucSvcMgr = (SiabucRemoteServiceManager) serviceManager;
		CreateUserResponseData requestItemResponseData = new CreateUserResponseData();
		
		List<Problem> problems = new ArrayList<Problem>();
		UserId patronUserId = initData.getUserId();
		
		if(patronUserId != null){
			if(siabucSvcMgr.lookupUserInfo(patronUserId.getUserIdentifierValue()) == null){
				UserOptionalFields userInformation = new UserOptionalFields();
				userInformation.setDateOfBirth(initData.getDateOfBirth());
				userInformation.setNameInformation(initData.getNameInformation());
				userInformation.setUserAddressInformations(initData.getUserAddressInformations());
				ArrayList<UserId> userIds = new ArrayList<UserId>();
				userIds.add(patronUserId);
				userInformation.setUserIds(userIds);
				
				try{
					siabucSvcMgr.registerUser(userInformation);
				}
				catch(ILSException ex){
					Problem problem = new Problem();
					problem.setProblemType(Version1GeneralProcessingError.TEMPORARY_PROCESSING_FAILURE);
					problem.setProblemDetail(ex.getMessage());
					problems.add(problem);
				}
				
			}
			else{
				Problem userFound = new Problem();
	    		userFound.setProblemType(Version1LookupUserProcessingError.NON_UNIQUE_USER);
	    		userFound.setProblemDetail("Ya existe el usuario");
	    		problems.add(userFound);
			}
		}
		else{
			Problem noUserFound = new Problem();
    		noUserFound.setProblemType(Version1GeneralProcessingError.NEEDED_DATA_MISSING);
    		noUserFound.setProblemDetail("Debe proveer el usuario para poder realizar una solicitud");
    		problems.add(noUserFound);
		}
			
		
		requestItemResponseData.setProblems(problems);
		
		return requestItemResponseData;
	}
		
	
	

	
}
