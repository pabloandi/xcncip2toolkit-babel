/**
 * Copyright (c) 2010 eXtensible Catalog Organization
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the MIT/X11 license. The text of the license can be
 * found at http://www.opensource.org/licenses/mit-license.php.
 */

package org.extensiblecatalog.ncip.v2.siabuc;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;


import org.apache.log4j.Logger;

import org.extensiblecatalog.ncip.v2.service.*;

//import org.extensiblecatalog.ncip.v2.siabuc.util.ILSConfiguration;
import org.extensiblecatalog.ncip.v2.siabuc.util.*;

import org.extensiblecatalog.ncip.v2.common.ConnectorConfigurationFactory;
import org.extensiblecatalog.ncip.v2.common.ConnectorConfiguration;


/**
 * ServiceManager is responsible for locating the correct back-end service.
 */
public class SiabucRemoteServiceManager implements RemoteServiceManager {
	
	static Logger log = Logger.getLogger(SiabucRemoteServiceManager.class);
	private ConnectorConfiguration siabucConfig;
	{
		try{
			siabucConfig = (ConnectorConfiguration)ConnectorConfigurationFactory.getConfiguration();
		}
		catch(ToolkitException e){
			throw new ExceptionInInitializerError(e);
		}
	}
	
	public SiabucRemoteServiceManager() throws ServiceException {
		
	} 
	
	public SiabucRemoteServiceManager(Properties properties) throws ServiceException {
		
	}
	
	/**
	 * Abre una conexión a la base de datos SIABUC9 postgresql
	 */
	private Connection openDbConnection(){
		
		Connection conn;
		
		log.debug("Entrando a openDbConnection()");
		
		
		try{
			Class.forName("org.postgresql.Driver");
			
			String url= (String) siabucConfig.getProperty(Constants.CONFIG_SIABUC_DB_URL);
			log.debug(url);
			String username= (String) siabucConfig.getProperty(Constants.CONFIG_SIABUC_DB_USERNAME);
			String password= (String) siabucConfig.getProperty(Constants.CONFIG_SIABUC_DB_PASSWORD);
			conn = DriverManager.getConnection(url, username, password);
			log.debug("Conexión a SIABUC9 = "+conn);
		} 
		catch(ClassNotFoundException ce){
			log.error("No se ha cargado el driver jdbc Postgresql", ce);
			return null;
		}
		catch(SQLException se){
			log.error("Error en la conexión con SIABUC9. Por favor asegurarse de tener los parámetros correctos de conexión", se);
			return null;
		}
		return conn;
	}
	
	/**
	 * Autenticar usuario
	 * 
	 * @param inputs
	 * @return
	 * @throws ILSException
	 */
	/*
	public String authenticateUser(List<AuthenticationInput> inputs){
		String username=null;
		String password=null;
		String authenticateUserId = null;
		
		
		for (AuthenticationInput a : inputs) {
			if (a.getAuthenticationInputType().getValue().equalsIgnoreCase("Username")) {
				username = a.getAuthenticationInputData();
			} else if (a.getAuthenticationInputType().getValue().equalsIgnoreCase("Password")) {
				password = a.getAuthenticationInputData();
			}
		}
		
		if(username != null && password != null){
			authenticateUserId = authenticateUser(username, password);
		}
		
	}
	*/
	
	/**
	 * Autenticar usuario via Postgresql
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws ILSException
	 */
	
	/*
	public String authenticateUser(String username, String password){
		
	}
	*/
	
	/**
	 * Buscar información del usuario por medio del identificador
	 * @param id
	 * @return
	 * @throws ILSException
	 */
	public Map<String, String>  lookupUserInfo(String id) {
		
		Map<String, String> user = new HashMap<String, String>(); 
		
		try{
			Connection con = openDbConnection();
			PreparedStatement pst = con.prepareStatement("Select * from catalogos.usuarios where no_cuenta = '?' limit 1");
			pst.setString(1,(String) id);
			ResultSet rs = pst.executeQuery();
			
			while(rs.next()){
				user.put("no_cuenta", rs.getString("no_cuenta"));
				user.put("nombre", rs.getString("nombre"));
				user.put("no_grupo", rs.getString("no_grupo"));
				user.put("idescuela", rs.getString("idescuela"));
				user.put("correo", rs.getString("correo"));
				user.put("domicilio", rs.getString("domicilio"));
				user.put("colonia", rs.getString("colonia"));
				user.put("ciudad_estado", rs.getString("ciudad_estado"));
				user.put("cod_pos", rs.getString("cod_pos"));
				user.put("telefono", rs.getString("telefono"));
				user.put("foto", rs.getString("foto"));
				user.put("notas", rs.getString("notas"));
				user.put("inicio_vigencia", rs.getString("inicio_vigencia"));
				user.put("fin_vigencia", rs.getString("fin_vigencia"));
				user.put("nip", rs.getString("nip"));
				user.put("analista", rs.getString("analista"));
				user.put("reserva_actuales", rs.getString("reserva_actuales"));
				user.put("idcarrera", rs.getString("idcarrera"));
				
			}
			
		} catch(SQLException ex){
			log.error("No se pudo consulta la información de usuario", ex);
		}
		
		return user;
		
	}
	
	/**
	 * Buscar Bloqueos por Usuario
	 * @param id
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 * 
	 */
	public List<Map<String, String>> lookupBlocksByUser(String id){
		
		List<Map<String, String>> blocks = new ArrayList<Map<String, String>>();
		
		try{
			Connection con = openDbConnection();
			PreparedStatement pst = con.prepareStatement("select * from prestamo.bloqueos where no_cuenta = '?'");
			pst.setString(1,(String) id);
			ResultSet rs = pst.executeQuery();
			
			while(rs.next()){
				Map<String, String> block = new HashMap<String, String>();
				
				block.put("idbloqueo", rs.getString("idbloqueo"));
				block.put("no_cuenta", rs.getString("no_cuenta"));
				block.put("idescuela", rs.getString("idescuela"));
				block.put("f_final", rs.getString("f_final"));
				block.put("observaciones", rs.getString("observaciones"));
				block.put("f_bloqueo", rs.getString("f_bloqueo"));
				block.put("analista", rs.getString("analista"));
				block.put("f_devolucion", rs.getString("f_devolucion"));
				block.put("f_prestamo", rs.getString("f_prestamo"));
				block.put("f_entrega", rs.getString("f_entrega"));
				block.put("monto", rs.getString("monto"));
				block.put("activo", rs.getString("activo"));

				
				blocks.add(block);
				
			}
			
		} catch(SQLException ex){
			log.error("No se pudo consultar los bloqueos del usuario", ex);
		}
		
		return blocks;
		
	}
	
	/**
	 * Buscar direcciones de contacto por Usuario
	 * @param id
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 */
	public UserAddressInformation lookupAddressesByUser(String id){
		UserAddressInformation address = new UserAddressInformation();
		
		
		try{
			Map<String, String> user = lookupUserInfo(id);
			PhysicalAddress physical_address = new PhysicalAddress();
			
			if(user.get("domicilio") != null || user.get("domicilio") != ""){
				UnstructuredAddress unstructured_address = new UnstructuredAddress();
				unstructured_address.setUnstructuredAddressData(user.get("domicilio"));
				
				physical_address.setUnstructuredAddress(unstructured_address);
				
				address.setPhysicalAddress(physical_address);
				
			}
			
			if(user.get("correo") != null || user.get("correo") != ""){
				ElectronicAddress email_address = new ElectronicAddress();
				email_address.setElectronicAddressData(user.get("correo"));
				
				
				address.setElectronicAddress(email_address);
			}
			
			
			
			
		}
		catch(NullPointerException npe){
			log.error("No se pudo consultar las direcciones del usuario",npe);
		}
		
		return address;
		
	}
	
	/**
	 * Buscar Información de nombre por Usuario
	 * @param id
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 */
	public NameInformation lookupNameInformationByUser(String id){
		NameInformation name = new NameInformation();
		
		
		try{
			Map<String, String> user = lookupUserInfo(id);
			
			
			if(user.get("nombre") != null || user.get("nombre") != ""){
				PersonalNameInformation p = new PersonalNameInformation();
				p.setUnstructuredPersonalUserName(user.get("nombre"));
				
				name.setPersonalNameInformation(p);
				
			}
			
			
			
		}
		catch(NullPointerException npe){
			log.error("No se pudo consultar las direcciones del usuario",npe);
		}
		
		return name;
	}
	
	/**
	 * Buscar Prestamos por Usuario
	 * @param id
	 * @param agencyId
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 * 
	 */
	public List<LoanedItem> lookupLoanedItemsByUser(String id, String agencyId){
		
		List<LoanedItem> loaned_items = new ArrayList<LoanedItem>();
		
		try{
			Connection con = openDbConnection();
			PreparedStatement pst = con.prepareStatement("select * from prestamo.prestamos where no_cuenta = ?");
			pst.setString(1,(String) id);
			ResultSet rs = pst.executeQuery();
			
			while(rs.next()){
				LoanedItem item = new LoanedItem();
								
				Timestamp dueDate = rs.getTimestamp("f_entrega");
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(dueDate);
				item.setDateDue(gc);
				item.setReminderLevel(new BigDecimal(1));
				
				ItemId itemId = new ItemId();
				itemId.setAgencyId(new AgencyId(agencyId));
				itemId.setItemIdentifierValue(rs.getString("no_adqui"));
				itemId.setItemIdentifierType(Version1ItemIdentifierType.ACCESSION_NUMBER);
				item.setItemId(itemId);
				
				item.setTitle(rs.getString("titulo"));
				
				BibliographicDescription bibliographicDescription = new BibliographicDescription();
				List<BibliographicRecordId> bibliographicRecordIds = new ArrayList<BibliographicRecordId>();
				BibliographicRecordId bibRecordId = new BibliographicRecordId();
				bibRecordId.setBibliographicRecordIdentifier(rs.getString("clasificacion"));
				bibRecordId.setAgencyId(new AgencyId(agencyId));	
				bibliographicRecordIds.add(bibRecordId);
				bibliographicDescription.setBibliographicRecordIds(bibliographicRecordIds);
				item.setBibliographicDescription(bibliographicDescription);
				
				Amount amount = new Amount();
				amount.setCurrencyCode(new CurrencyCode("COP", 1000));
				amount.setMonetaryValue(new BigDecimal(0));
				item.setAmount(amount);
				
				loaned_items.add(item);
				
			}
			
			
			
			
		} catch(SQLException ex){
			log.error("No se pudo consultar los bloqueos del usuario", ex);
		}
		
		return loaned_items;
		
	}
	
	/**
	 * Buscar Solicitudes de Prestamos por Usuario
	 * @param id
	 * @param agencyId
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 * 
	 */
	public List<RequestedItem> lookupRequestedItemsByUser(String id, String agencyId){
		
		List<RequestedItem> requested_items = new ArrayList<RequestedItem>();
		
		try{
			Connection con = openDbConnection();
			PreparedStatement pst = con.prepareStatement("select * from prestamo.reservaciones where NO_CUENTA = ?");
			pst.setString(1,(String) id);
			ResultSet rs = pst.executeQuery();
			
			while(rs.next()){
				RequestedItem item = new RequestedItem();
				
				Timestamp expireDate = rs.getTimestamp("fecha_fin");
				GregorianCalendar edgc = new GregorianCalendar();
				edgc.setTime(expireDate);
				item.setPickupExpiryDate(edgc);
				item.setReminderLevel(new BigDecimal(1));
				
				Timestamp pickupDate = rs.getTimestamp("fecha_inicio");
				GregorianCalendar pgc = new GregorianCalendar();
				pgc.setTime(pickupDate);
				item.setPickupDate(pgc);
				
				Timestamp datePlaced = rs.getTimestamp("fecha_aviso");
				GregorianCalendar dpgc = new GregorianCalendar();
				if(datePlaced!=null){
					dpgc.setTime(datePlaced);
					item.setDatePlaced(dpgc);
				}
				else{
					item.setDatePlaced(dpgc);
				}
				
				
				
				ItemId itemId = new ItemId();
				itemId.setAgencyId(new AgencyId(agencyId));
				itemId.setItemIdentifierValue(rs.getString("no_adqui"));
				itemId.setItemIdentifierType(Version1ItemIdentifierType.ACCESSION_NUMBER);
				item.setItemId(itemId);
				
				item.setTitle(rs.getString("titulo"));
				
				BibliographicDescription bibliographicDescription = new BibliographicDescription();
				List<BibliographicRecordId> bibliographicRecordIds = new ArrayList<BibliographicRecordId>();
				BibliographicRecordId bibRecordId = new BibliographicRecordId();
				bibRecordId.setBibliographicRecordIdentifier(rs.getString("clasificacion"));
				bibRecordId.setAgencyId(new AgencyId(agencyId));	
				bibliographicRecordIds.add(bibRecordId);
				bibliographicDescription.setBibliographicRecordIds(bibliographicRecordIds);
				item.setBibliographicDescription(bibliographicDescription);
				
				item.setRequestType(XcRequestType.HOLD);
				
				if(rs.getBoolean("activa"))
					item.setRequestStatusType(new RequestStatusType("activo"));
				else
					item.setRequestStatusType(new RequestStatusType("inactivo"));
				
				
				
				requested_items.add(item);
				
			}
			
			return requested_items;
			
			
		} catch(SQLException ex){
			log.error("No se pudo consultar las peticiones de material del usuario", ex);
		} catch(NullPointerException npe){
			log.error("No se pudo consultar las peticiones de material del usuario", npe);
			return requested_items;
		}
		
		return null;
		
	}
	
	/**
	 * Buscar descripción bibliográfica por id bibliográfico
	 * @param bibId
	 * @return 
	 * @author pabloadi
	 * @throws ILSException
	 */
	
	public BibliographicDescription lookupBibliographicDescriptionByBibId(BibliographicId bibId) throws ILSException {
		
		BibliographicDescription bibDescription = new BibliographicDescription();
		PreparedStatement pst;
		ResultSet rs = null;
		
		try{
			Connection con = openDbConnection();
			
			
			if(bibId.getBibliographicItemId().getBibliographicItemIdentifier() != null){
				
				if(bibId.getBibliographicItemId().getBibliographicItemIdentifierCode().matches(Version1BibliographicItemIdentifierCode.ISBN) 
						|| bibId.getBibliographicItemId().getBibliographicItemIdentifierCode().matches(Version1BibliographicItemIdentifierCode.ISSN)){
					pst = con.prepareStatement("select * from analisis.fichas where isbn = ?");
					pst.setString(1,(String) bibId.getBibliographicItemId().getBibliographicItemIdentifier());
					
					rs = pst.executeQuery();
					
				}
				
			}
			else if(bibId.getBibliographicRecordId().getBibliographicRecordIdentifier() != null){
				
				if(bibId.getBibliographicRecordId().getBibliographicRecordIdentifierCode().matches(Version1BibliographicRecordIdentifierCode.CN)){
					pst = con.prepareStatement("select * from analisis.fichas where ficha_no = ? and idbiblioteca = ?");
					pst.setString(1,(String) bibId.getBibliographicRecordId().getBibliographicRecordIdentifier());
					pst.setString(2,(String) bibId.getBibliographicRecordId().getAgencyId().getValue());
					
					rs = pst.executeQuery();
				}
			}
			

			
			
			while(rs.next()){
				bibDescription.setAuthor(rs.getString("autor"));
				bibDescription.setTitle(rs.getString("titulo"));
				bibDescription.setPublicationDate(rs.getString("fecha1"));
				bibDescription.setPlaceOfPublication(rs.getString("lugarpub"));
				//TODO No está claro la forma de establecer el lenguaje
				bibDescription.setLanguage(Version1Language.find(Version1Language.VERSION_1_LANGUAGE, rs.getString("idiomapub")));
				if(rs.getString("tipoficha").equals("L")){
					bibDescription.setMediumType(Version1MediumType.BOOK);
				}
				else if(rs.getString("tipoficha").equals("R") || rs.getString("tipoficha").equals("A")){
					bibDescription.setMediumType(Version1MediumType.MAGAZINE);
				}
				bibDescription.setPublisher(rs.getString("editorial"));
				
				
			}
		}
		catch(SQLException ex){
			log.error("Hubo un error en la consulta de fichas", ex);
			throw new ILSException(ex.getMessage(), ex.getCause());
		}
		catch(ServiceException ex){
			log.error("Hubo un error en la selección del lenguaje para la ficha", ex);
			throw new ILSException(ex.getMessage(), ex.getCause());
		}
		
		return bibDescription;
	}
	
	
	/**
	 * Buscar descripción bibliográfica por id del material
	 * @param bibId
	 * @return 
	 * @author pabloadi
	 * @throws ILSException
	 */
	
	public BibliographicDescription lookupBibliographicDescriptionByItemId(ItemId itemId) throws ILSException {
		
		BibliographicDescription bibDescription = new BibliographicDescription();
		PreparedStatement pst;
		ResultSet rs;
				
		try{
			Connection con = openDbConnection();
			
			
			if(itemId.getItemIdentifierValue() != null){
				
				pst = con.prepareStatement("select f.* from analisis.fichas f join analisis.ejemplares e on f.idficha = e.idficha where e.no_adqui = ? and e.idbiblioteca = ?");
				pst.setString(1,(String) itemId.getItemIdentifierValue());
				pst.setString(1,(String) itemId.getAgencyId().getValue());
				
				rs = pst.executeQuery();
				
				while(rs.next()){
					bibDescription.setAuthor(rs.getString("autor"));
					bibDescription.setTitle(rs.getString("titulo"));
					bibDescription.setPublicationDate(rs.getString("fecha1"));
					bibDescription.setPlaceOfPublication(rs.getString("lugarpub"));
					//TODO No está claro la forma de establecer el lenguaje
					bibDescription.setLanguage(Version1Language.find(Version1Language.VERSION_1_LANGUAGE, rs.getString("idiomapub")));
					if(rs.getString("tipoficha").equals("L")){
						bibDescription.setMediumType(Version1MediumType.BOOK);
					}
					else if(rs.getString("tipoficha").equals("R") || rs.getString("tipoficha").equals("A")){
						bibDescription.setMediumType(Version1MediumType.MAGAZINE);
					}
					bibDescription.setPublisher(rs.getString("editorial"));
					
					List<BibliographicRecordId> bibliographicRecordIds = new ArrayList<BibliographicRecordId>();
					BibliographicRecordId bibRecordId = new BibliographicRecordId();
					bibRecordId.setBibliographicRecordIdentifier(rs.getString("clasificacion"));
					bibRecordId.setBibliographicRecordIdentifierCode(Version1BibliographicRecordIdentifierCode.CN);
					bibliographicRecordIds.add(bibRecordId);
					bibDescription.setBibliographicRecordIds(bibliographicRecordIds);
					
					
				}
			}
			
		}
		catch(SQLException ex){
			log.error("Hubo un error en la consulta de fichas", ex);
			throw new ILSException(ex.getMessage(), ex.getCause());
		}
		catch(ServiceException ex){
			throw new ILSException(ex.getMessage(),ex.getCause());
		}
		
		
		
		return bibDescription;
	}
	
	/**
	 * Buscar HoldingSet por id bibliográfico
	 * @param bibId
	 * @param initData
	 * @return
	 * @author pabloadi
	 * @throws ILSException 
	 */
	
	public HoldingsSet lookupHoldingsSetByBidId(BibliographicId bibId, LookupItemSetInitiationData initData) throws ILSException {
		
		HoldingsSet holdingSet = new HoldingsSet();
		List<ItemInformation> itemInformations = new ArrayList<ItemInformation>();
		
		try{
			Connection con = openDbConnection();
			PreparedStatement pst = null;
			
			if(bibId.getBibliographicItemId().getBibliographicItemIdentifier() != null){
				
				if(bibId.getBibliographicItemId().getBibliographicItemIdentifierCode().matches(Version1BibliographicItemIdentifierCode.ISBN) 
						|| bibId.getBibliographicItemId().getBibliographicItemIdentifierCode().matches(Version1BibliographicItemIdentifierCode.ISSN)){
					pst = con.prepareStatement("select e.* from analisis.ejemplares e join analisis.fichas f on e.idficha = f.idficha where f.isbn = ?");
					pst.setString(1,(String) bibId.getBibliographicItemId().getBibliographicItemIdentifier());
					
				}
				
			}
			else if(bibId.getBibliographicRecordId().getBibliographicRecordIdentifier() != null){
				
				if(bibId.getBibliographicRecordId().getBibliographicRecordIdentifierCode().matches(Version1BibliographicRecordIdentifierCode.CN)){
					pst = con.prepareStatement("select * from analisis.ejemplares where ficha_no = ?");
					pst.setString(1,(String) bibId.getBibliographicRecordId().getBibliographicRecordIdentifier());
				}
			}
			
			ResultSet rs = pst.executeQuery();
			
			while(rs.next()){
				
				ItemInformation itemInfo = new ItemInformation();
				
				ItemId itemId = new ItemId();
				AgencyId agencyId = new AgencyId(rs.getString("idbiblioteca"));
				itemId.setAgencyId(agencyId);
				itemId.setItemIdentifierValue(rs.getString("no_adqui"));
				itemId.setItemIdentifierType(Version1ItemIdentifierType.ACCESSION_NUMBER);
				ItemOptionalFields itemOptionals = new ItemOptionalFields();
				
				if(initData.getCirculationStatusDesired()){
					
					itemOptionals.setCirculationStatus(checkCirculationStatusByItemId(itemId,agencyId)); 
				}
				
				if(initData.getCurrentBorrowerDesired()){
					try{
						itemInfo.setCurrentBorrower(checkCurrentBorrowerByItemId(itemId,agencyId));
					}
					catch(ILSException ex){
						
						throw new ILSException(ex.getMessage(), ex.getCause());
					}
				}
				
				if(initData.getCurrentRequestersDesired()){
					
				}
				
				if(initData.getElectronicResourceDesired()){
					
				}
				
				if(initData.getHoldQueueLengthDesired()){
					
				}
				
				if(initData.getItemDescriptionDesired()){
					
				}
				
				if(initData.getItemUseRestrictionTypeDesired()){
					
				}
				
				if(initData.getLocationDesired()){
					
				}
				
				if(initData.getPhysicalConditionDesired()){
					
				}
				
				if(initData.getSecurityMarkerDesired()){
					
				}
				
				if(initData.getBibliographicDescriptionDesired()){
					
					itemOptionals.setBibliographicDescription(lookupBibliographicDescriptionByBibId(bibId));
					
					
				}
								
				itemInfo.setItemOptionalFields(itemOptionals);
				itemInformations.add(itemInfo);
				
				
			}
			
			holdingSet.setItemInformations(itemInformations);
			
		}
		catch(SQLException ex){
			throw new ILSException(ex.getMessage(), ex.getCause());
		}
		
		
		return holdingSet;
	}
	
	/**
	 * Verificar estatus de circulación por id de item
	 * @param itemId
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 */
	
	public Version1CirculationStatus checkCirculationStatusByItemId(ItemId itemId, AgencyId agencyId) throws ILSException{
		
		Connection con;
		Version1CirculationStatus circulation_status = Version1CirculationStatus.AVAILABLE_ON_SHELF;
		
		
		try{
		    con = openDbConnection();
			PreparedStatement pst = con.prepareStatement("select * from prestamo.prestamos where no_adqui = ? and idbiblioteca = ?");
			pst.setString(1,(String) itemId.getItemIdentifierValue());
			pst.setString(2,(String) agencyId.getValue());
			ResultSet rs = pst.executeQuery();
			
			if(rs.first()){
				circulation_status = Version1CirculationStatus.ON_LOAN;
			}
		}
		catch(SQLException ex){
			throw new ILSException(ex.getMessage(), ex.getCause());
		}
		
		
		try{
		    con = openDbConnection();
			PreparedStatement pst = con.prepareStatement("select * from prestamo.reservaciones where no_adqui = ? and idbiblioteca = ?");
			pst.setString(1,(String) itemId.getItemIdentifierValue());
			pst.setString(2,(String) agencyId.getValue());
			ResultSet rs = pst.executeQuery();
			
			if(rs.first()){
				circulation_status = Version1CirculationStatus.AVAILABLE_FOR_PICKUP;
			}
		}
		catch(SQLException ex){
			throw new ILSException(ex.getMessage(), ex.getCause());
		}
		
		try{
		    con = openDbConnection();
			PreparedStatement pst = con.prepareStatement("select * from catalogos.mat_especial where no_adqui = ? and biblioteca = ?");
			pst.setString(1,(String) itemId.getItemIdentifierValue());
			pst.setString(2,(String) agencyId.getValue());
			ResultSet rs = pst.executeQuery();
			
			while(rs.next()){
				String estado = rs.getString("estado").trim();
				
				if(estado.equals("16") || estado.equals("17") || estado.equals("18") 
						|| estado.equals("19")  || estado.equals("20") || estado.equals("21")
							 || estado.equals("22") || estado.equals("23") || estado.equals("24")
							 	 || estado.equals("25")  || estado.equals("28") || estado.equals("29")
						){
					circulation_status = Version1CirculationStatus.IN_PROCESS;
				}
				
				if(estado.equals("1") || estado.equals("3") || estado.equals("5") 
						|| estado.equals("7")  || estado.equals("9") || estado.equals("13")
							 || estado.equals("17") || estado.equals("19") || estado.equals("21")
							 	 || estado.equals("23")  || estado.equals("25") || estado.equals("29")
						){
					circulation_status = Version1CirculationStatus.MISSING;
				}
				
				
				
			}
			
			
		}
		catch(SQLException ex){
			throw new ILSException(ex.getMessage(), ex.getCause());
		}
		
		
		//TODO Establecer mas estados de circulación propuestos por SIABUC
		
		return circulation_status;
	}
	
	
	/**
	 * Verificar usuario prestante del elemento por id de item
	 * @param itemId
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 */
	
	public CurrentBorrower checkCurrentBorrowerByItemId(ItemId itemId, AgencyId agencyId) throws ILSException {
		
		Connection con;
		CurrentBorrower current_borrower = null;
		
		
		try{
		    con = openDbConnection();
			PreparedStatement pst = con.prepareStatement("select u.* from catalogos.usuarios u join prestamo.prestamos on where no_adqui = ? and idbiblioteca = ?");
			pst.setString(1,(String) itemId.getItemIdentifierValue());
			pst.setString(2,(String) agencyId.getValue());
			ResultSet rs = pst.executeQuery();
			
			if(rs.first()){
				current_borrower = new CurrentBorrower();
				UserId userId = new UserId();
				userId.setUserIdentifierValue(rs.getString("no_cuenta"));
				userId.setUserIdentifierType(Version1UserIdentifierType.INSTITUTION_ID_NUMBER);
				userId.setAgencyId(agencyId);
				current_borrower.setUserId(userId);
			}
		}
		catch(SQLException ex){
			log.error("Hubo un error en la consulta del usuario prestante", ex);
			throw new ILSException(ex.getMessage(),ex.getCause());
		}
		
		return current_borrower;
		
	}
	
	public boolean registerUser(UserOptionalFields userInfo) throws ILSException {
		
		Connection con;
		PreparedStatement pst = null;
		java.util.Calendar fin_vigencia = Calendar.getInstance();
		fin_vigencia.add(Calendar.YEAR, 1);
		
		try{
			con = openDbConnection();
			pst = con.prepareStatement("insert into catalogos.usuarios " +
					"(no_cuenta,nombre,no_grupo,idescuela,correo,domicilio,notas,inicio_vigencia,fin_vigencia,nip,analista) " +
					"values (?,?,?,?,?,?,?,?,?)");
			pst.setString(0, userInfo.getUserId(0).getUserIdentifierValue());
			pst.setString(1, userInfo.getNameInformation().getPersonalNameInformation().toString());
			pst.setInt(3, 1); //TODO establecer no_grupo
			pst.setInt(4, 1); //TODO establecer idescuela
			pst.setString(5, userInfo.getUserAddressInformation(0).getElectronicAddress().getElectronicAddressData());
			pst.setString(6, userInfo.getUserAddressInformation(0).getPhysicalAddress().getUnstructuredAddress().getUnstructuredAddressData());
			pst.setString(7, "usuario creado por el servicio de NCIP de la plataforma BABEL");
			pst.setDate(8, new java.sql.Date(Calendar.getInstance().getTime().getTime()));
			pst.setDate(9, new java.sql.Date(fin_vigencia.getTime().getTime()));
			pst.setString(10, userInfo.getUserId(0).getUserIdentifierValue());
			pst.setString(11, "NCIP-BABEL");
			
			pst.executeUpdate();
			
		}
		catch(SQLException ex){
			log.error("Hubo un error en la registro del usuario", ex);
			throw new ILSException(ex.getMessage(), ex.getCause());
			
		}
		
		return true;
	}
	
	
	/**
	 * Realizar solicitud de prestamo de un elemento por id de item
	 * @param initData
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 * @throws SQLException 
	 */
	
	public void requestItems(RequestItemInitiationData initData) throws ILSException, NoUserFoundException, ItemNotAvailableException, NoItemFoundException, SQLException {
		
		
		Map<String, String> user = lookupUserInfo(initData.getUserId().getUserIdentifierValue());
		UserId userId = null;
		
		
		if(user != null){
			userId = new UserId();
			userId.setAgencyId(new AgencyId((String) Constants.CONFIG_ILS_DEFAULT_AGENCY));
			userId.setUserIdentifierValue(user.get("no_cuenta"));
			userId.setUserIdentifierType(Version1UserIdentifierType.BARCODE);
			
		}
		else{
			log.info("No existe el usuario en la base de datos de SIABUC9, remitiendo al servicio de creación de usuario ...");
			throw new NoUserFoundException("No existe el usuario en la base de datos de SIABUC9, por favor solicite la creación del usuario por medio de NCIP ...");
		}
				
		for(ItemId itemId : initData.getItemIds()){
			
		
			// verificar el estado de circulación del item
			Version1CirculationStatus circulation_status = checkCirculationStatusByItemId(itemId, itemId.getAgencyId());
			BibliographicDescription bib_description = lookupBibliographicDescriptionByItemId(itemId); 
			
			if(circulation_status != Version1CirculationStatus.AVAILABLE_ON_SHELF){
				log.info("El material no está disponible para reserva");
				throw new ItemNotAvailableException(circulation_status.toString());
			}
			
			if(bib_description == null){
				log.info("No existe descripción bibliográfica del material. No se puede reservar el material");
				throw new NoItemFoundException("No existe descripción del material, se puede decir que el material no existe");
			}
			
						
			Connection con = null;
			PreparedStatement pst = null;
			
			try{
			    con = openDbConnection();
				pst = con.prepareStatement("insert into prestamo.reservaciones " +
						"(idbiblioteca, no_adqui, no_cuenta, fecha_fin, fecha_inicio, web, analista_reservo, titulo, autor, clasificacion, usuario, observaciones)" +
						"values " +
						"(?,?,?,?,?,?,?,?,?,?,?,?)");
				pst.setString(1,(String) Constants.CONFIG_ILS_DEFAULT_AGENCY);
				pst.setString(2,(String) itemId.getItemIdentifierValue());
				pst.setString(3,(String) userId.getUserIdentifierValue());
				
				pst.setDate(4, new java.sql.Date(initData.getPickupExpiryDate().getTimeInMillis()));
				pst.setDate(5, new java.sql.Date(initData.getPickupDate().getTimeInMillis()));
				pst.setBoolean(6, true);
				pst.setString(7, "BABEL");
				pst.setString(8, bib_description.getTitle());
				pst.setString(9, bib_description.getAuthor());
				pst.setString(10, bib_description.getBibliographicRecordId(0).getBibliographicRecordIdentifier());
				pst.setString(11, userId.getUserIdentifierValue());
				pst.setString(12, "Préstamo interbibliotecario: El usuario es: \n"
						+ userId.getUserIdentifierValue()
						+ "\n de la biblioteca o institución universitaria: \n"
						+ userId.getAgencyId().getValue()
						+ "\n desea solicitar el siguiente material: \n"
						+ itemId.getItemIdentifierValue()
						+ "con tipo de identificación de: \n"
						+ itemId.getItemIdentifierType()
						+ " de la biblioteca: \n"
						+ itemId.getAgencyId().getValue());
				
				
				pst.executeUpdate();
				

			}
			catch(SQLException ex){
				log.error("Hubo un error en el registro de la solicitud de préstamo", ex);
				throw new ILSException(ex.getMessage());
			}
			
		}
		
	}
	
	
	
	
	
}

