/**
 * Copyright (c) 2010 eXtensible Catalog Organization
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the MIT/X11 license. The text of the license can be
 * found at http://www.opensource.org/licenses/mit-license.php.
 */

package org.extensiblecatalog.ncip.v2.koha;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


import org.apache.log4j.Logger;
import org.extensiblecatalog.ncip.v2.koha.util.Constants;
import org.extensiblecatalog.ncip.v2.service.*;


/**
 * This class implements Lookup User service for siabuc9 connector
 * @author pabloadi
 *
 */
public class KohaLookupUserService implements LookupUserService {
	
	static Logger log = Logger.getLogger(KohaLookupUserService.class);
	KohaRemoteServiceManager siabucSvcMgr;
	
	
	@Override
    public LookupUserResponseData performService(LookupUserInitiationData initData,
                                          ServiceContext serviceContext,
                                          RemoteServiceManager serviceManager)
        throws ServiceException {
		
		siabucSvcMgr = (KohaRemoteServiceManager) serviceManager;
		LookupUserResponseData lookupUserResponseData = new LookupUserResponseData();
		
		String patronId = initData.getUserId().getUserIdentifierValue();
		List<Problem> problems = new ArrayList<Problem>();
		
		if(initData.getUserId() != null){
			log.info("Id del usuario es "+initData.getUserId().getUserIdentifierValue());
					
			if(siabucSvcMgr.lookupUserInfo(patronId) != null){
				UserId userId = new UserId();
				userId.setUserIdentifierValue(patronId);
				
				lookupUserResponseData.setUserId(userId);
				
				// información de nombre y direcciones del usuario
				if(initData.getUserAddressInformationDesired() || initData.getNameInformationDesired()){
									
					UserOptionalFields userOptionalFields = new UserOptionalFields();
					
					
					if(initData.getUserAddressInformationDesired()){
						log.debug("Entrando a información de direcciones de usuario");
						
						List<UserAddressInformation> userAddressInformations = new ArrayList<UserAddressInformation>();
						userAddressInformations.add(siabucSvcMgr.lookupAddressesByUser(patronId));
						userOptionalFields.setUserAddressInformations(userAddressInformations);
					}
					
					if(initData.getNameInformationDesired()){
						log.debug("Entrando a información de nombre de usuario");
						
						NameInformation nameInfo = siabucSvcMgr.lookupNameInformationByUser(patronId);
						if(nameInfo!=null)
							userOptionalFields.setNameInformation(nameInfo);
					}
					
					lookupUserResponseData.setUserOptionalFields(userOptionalFields);
				}
				
				// información de multas del usuario
				if(initData.getUserFiscalAccountDesired()){
					log.debug("Entrando a información de multas de usuario");
				}
				
				// información de solicitudes de préstamo del usuario
				if(initData.getRequestedItemsDesired()){
					log.debug("Entrando a solicitudes de préstamo");
					
					List<RequestedItem> requested_items = siabucSvcMgr.lookupRequestedItemsByUser(patronId, Constants.CONFIG_ILS_DEFAULT_AGENCY);
					if(requested_items!=null){
						lookupUserResponseData.setRequestedItems(requested_items);
						
						if(requested_items.size()>0){
							List<RequestedItemsCount> requestedItemsCounts = new ArrayList<RequestedItemsCount>();
							RequestedItemsCount itemsHoldCount = new RequestedItemsCount();
							itemsHoldCount.setRequestType(XcRequestType.HOLD);
							itemsHoldCount.setRequestedItemCountValue(BigDecimal.valueOf(requested_items.size()));
							requestedItemsCounts.add(itemsHoldCount);
							lookupUserResponseData.setRequestedItemsCounts(requestedItemsCounts);
						}
					}
						
				}
				
				// información de elementos prestados del usuario
				if(initData.getLoanedItemsDesired()){
					log.debug("Entrando a información de elementos prestados");
					List<LoanedItem> loaned_items = siabucSvcMgr.lookupLoanedItemsByUser(patronId, Constants.CONFIG_ILS_DEFAULT_AGENCY);
					if(loaned_items!=null){
						lookupUserResponseData.setLoanedItems(loaned_items);
						if(loaned_items.size()>0){
							
							//lookupUserResponseData.setLoanedItemsCounts(loaned_items);
						}
						
					}
				}
			
			}
			else{
				problems.add(new Problem(Version1RequestItemProcessingError.UNKNOWN_USER,"UserId","No existe el usuario en el sistema"));
				lookupUserResponseData.setProblems(problems);
			}
			
		}
		else{
			problems.add(new Problem(Version1GeneralProcessingError.NEEDED_DATA_MISSING,"UserId","No se ha proporcionado la identificación del usuario a consultar"));
			lookupUserResponseData.setProblems(problems);
		}
		
		return lookupUserResponseData;
		
	}
	
	

}
