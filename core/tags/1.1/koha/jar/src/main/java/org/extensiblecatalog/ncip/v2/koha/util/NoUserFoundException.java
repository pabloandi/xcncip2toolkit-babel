package org.extensiblecatalog.ncip.v2.koha.util;

public class NoUserFoundException extends ILSException{
	
	/** Serial Id	 */
	private static final long serialVersionUID = -5767614856650456754L;
	
	/**
     * Construct a new AuthenticationException
     *
     * @param explanation the text message
     * @param cause       the original exception
     */
    public NoUserFoundException(String explanation, Throwable cause) {
        super(explanation, cause);
    }

    /**
     * Construct a new AuthenticationException
     *
     * @param explanation the text message
     */
    public NoUserFoundException(String explanation) {
        super(explanation);
    }

    /**
     * Construct a new AuthenticationException
     *
     * @param cause the original exception
     */
    public NoUserFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * Represent the exception as a String (for logging, etc.).
     *
     * @return the String representation
     */
    public String toString() {
        return super.toString();
    }
}