/**
 * Copyright (c) 2010 eXtensible Catalog Organization
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the MIT/X11 license. The text of the license can be
 * found at http://www.opensource.org/licenses/mit-license.php.
 */

package org.extensiblecatalog.ncip.v2.koha;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import org.apache.log4j.Logger;
import org.extensiblecatalog.ncip.v2.koha.util.*;
import org.extensiblecatalog.ncip.v2.service.*;

/**
 * This class implements Lookup Item service for siabuc9 connector
 * @author pabloadi
 *
 */
public class KohaRequestItemService implements RequestItemService {
	
	static Logger log = Logger.getLogger(KohaRequestItemService.class);
	KohaRemoteServiceManager siabucSvcMgr;
	
	@Override
	public RequestItemResponseData performService(
			RequestItemInitiationData initData,
			ServiceContext serviceContext, RemoteServiceManager serviceManager)
			throws ServiceException {
		// TODO Auto-generated method stub
		
		siabucSvcMgr = (KohaRemoteServiceManager) serviceManager;
		RequestItemResponseData requestItemResponseData = new RequestItemResponseData();
		
		BibliographicId bibliographicId = initData.getBibliographicId(0);
        String bibId = bibliographicId.getBibliographicRecordId().getBibliographicRecordIdentifier();
        String itemAgencyId = bibliographicId.getBibliographicRecordId().getAgencyId().getValue();
		
		String patronId;
        String patronAgencyId = (String) Constants.CONFIG_ILS_DEFAULT_AGENCY;
        List<Problem> problems = new ArrayList<Problem>();
        
        if (initData.getUserId() != null){
        	log.info("User id is " + initData.getUserId().getUserIdentifierValue());
        	patronId = initData.getUserId().getUserIdentifierValue();
        	if (initData.getUserId().getAgencyId() != null) {
        		patronAgencyId = initData.getUserId().getAgencyId().getValue();
        	}
        	else {
        		log.debug("No To Agency ID found in the initiation header");
        	}
        	
        	try{
        		siabucSvcMgr.requestItems(initData);
        	}
        	catch(NoUserFoundException ex){
        		Problem noUserFound = new Problem();
        		noUserFound.setProblemType(Version1RequestItemProcessingError.UNKNOWN_USER);
        		noUserFound.setProblemDetail(ex.getMessage());
        		problems.add(noUserFound);
        	}
        	catch(NoItemFoundException ex){
        		Problem noItemFound = new Problem();
        		noItemFound.setProblemType(Version1RequestItemProcessingError.UNKNOWN_ITEM);
        		noItemFound.setProblemDetail(ex.getMessage());
        		problems.add(noItemFound);
        	}
        	catch(ItemNotAvailableException ex){
        		Problem itemNotAvailable = new Problem();
        		itemNotAvailable.setProblemType(Version1RequestItemProcessingError.ITEM_NOT_AVAILABLE_BY_NEED_BEFORE_DATE);
        		itemNotAvailable.setProblemDetail(ex.getMessage());
        		problems.add(itemNotAvailable);
        	}
        	catch(ILSException ex){
        		Problem itemNotAvailable = new Problem();
        		itemNotAvailable.setProblemType(Version1GeneralProcessingError.TEMPORARY_PROCESSING_FAILURE);
        		itemNotAvailable.setProblemDetail(ex.getMessage());
        		problems.add(itemNotAvailable);
        	}
        	catch(SQLException ex){
        		Problem itemNotAvailable = new Problem();
        		itemNotAvailable.setProblemType(Version1GeneralProcessingError.TEMPORARY_PROCESSING_FAILURE);
        		itemNotAvailable.setProblemDetail(ex.getMessage());
        		problems.add(itemNotAvailable);
        	}
        	
        	
        	UserId userId = new UserId();
            userId.setUserIdentifierValue(patronId);
            userId.setUserIdentifierType(Version1UserIdentifierType.BARCODE);
            userId.setAgencyId(new AgencyId(patronAgencyId));
            requestItemResponseData.setUserId(userId);
            
            
            
            log.info("Patron ID: " + patronId);
            log.info("Patron Agency ID: " + patronAgencyId);
            log.info("Bibliographic Record ID: " + bibId);
            log.info("Bib Agency ID: " + itemAgencyId);
            log.info("Request Type: " + initData.getRequestType().getValue());
            log.info("Request Scope Type: " + initData.getRequestScopeType().getValue());
        }
        else{
        	Problem noUserFound = new Problem();
    		noUserFound.setProblemType(Version1GeneralProcessingError.NEEDED_DATA_MISSING);
    		noUserFound.setProblemDetail("Debe proveer el usuario para poder realizar una solicitud");
    		problems.add(noUserFound);
        }
		
        
		requestItemResponseData.setProblems(problems);
		
		return requestItemResponseData;
	}
		
	
	

	
}
