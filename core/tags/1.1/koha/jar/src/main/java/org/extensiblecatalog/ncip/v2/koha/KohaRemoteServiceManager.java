/**
 * Copyright (c) 2010 eXtensible Catalog Organization
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the MIT/X11 license. The text of the license can be
 * found at http://www.opensource.org/licenses/mit-license.php.
 */

package org.extensiblecatalog.ncip.v2.koha;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;


import org.apache.log4j.Logger;

import org.extensiblecatalog.ncip.v2.service.*;
import org.extensiblecatalog.ncip.v2.common.ConnectorConfigurationFactory;
import org.extensiblecatalog.ncip.v2.koha.util.*;
import org.extensiblecatalog.ncip.v2.common.ConnectorConfiguration;


/**
 * ServiceManager is responsible for locating the correct back-end service.
 */
public class KohaRemoteServiceManager implements RemoteServiceManager {
	
	static Logger log = Logger.getLogger(KohaRemoteServiceManager.class);
	private ConnectorConfiguration kohaConfig;
	{
		try{
			kohaConfig = (ConnectorConfiguration)ConnectorConfigurationFactory.getConfiguration();
		}
		catch(ToolkitException e){
			throw new ExceptionInInitializerError(e);
		}
	}
	
	public KohaRemoteServiceManager() throws ServiceException {
		
	} 
	
	public KohaRemoteServiceManager(Properties properties) throws ServiceException {
		
	}
	
	/**
	 * Abre una conexión a la base de datos KOHA postgresql
	 */
	private Connection openDbConnection(){
		
		Connection conn;
		
		log.debug("Entrando a openDbConnection()");
		
		
		try{
			Class.forName("com.mysql.jdbc.driver");
			
			String url= (String) kohaConfig.getProperty(Constants.CONFIG_KOHA_DB_URL);
			log.debug(url);
			String username= (String) kohaConfig.getProperty(Constants.CONFIG_KOHA_DB_USERNAME);
			String password= (String) kohaConfig.getProperty(Constants.CONFIG_KOHA_DB_PASSWORD);
			conn = DriverManager.getConnection(url, username, password);
			log.debug("Conexión a KOHA = "+conn);
		} 
		catch(ClassNotFoundException ce){
			log.error("No se ha cargado el driver jdbc Postgresql", ce);
			return null;
		}
		catch(SQLException se){
			log.error("Error en la conexión con KOHA. Por favor asegurarse de tener los parámetros correctos de conexión", se);
			return null;
		}
		return conn;
	}
	
	/**
	 * Autenticar usuario
	 * 
	 * @param inputs
	 * @return
	 * @throws ILSException
	 */
	/*
	public String authenticateUser(List<AuthenticationInput> inputs){
		String username=null;
		String password=null;
		String authenticateUserId = null;
		
		
		for (AuthenticationInput a : inputs) {
			if (a.getAuthenticationInputType().getValue().equalsIgnoreCase("Username")) {
				username = a.getAuthenticationInputData();
			} else if (a.getAuthenticationInputType().getValue().equalsIgnoreCase("Password")) {
				password = a.getAuthenticationInputData();
			}
		}
		
		if(username != null && password != null){
			authenticateUserId = authenticateUser(username, password);
		}
		
	}
	*/
	
	/**
	 * Autenticar usuario via Postgresql
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws ILSException
	 */
	
	/*
	public String authenticateUser(String username, String password){
		
	}
	*/
	
	/**
	 * Buscar información del usuario por medio del identificador
	 * @param id
	 * @return
	 * @throws ILSException
	 */
	public Map<String, String>  lookupUserInfo(String id) {
		
		Map<String, String> user = new HashMap<String, String>(); 
		
		try{
			Connection con = openDbConnection();
			PreparedStatement pst = con.prepareStatement("Select * from borrowers where cardnumber = '?' limit 1");
			pst.setString(1,(String) id);
			ResultSet rs = pst.executeQuery();
			
			while(rs.next()){
				user.put("cardnumber", rs.getString("cardnumber"));
				user.put("fullname", rs.getString("firstname")+" "+rs.getString("surname"));
				user.put("email", rs.getString("email"));
				user.put("address", rs.getString("address"));
				user.put("country", rs.getString("country"));
				user.put("city", rs.getString("city"));
				user.put("zipcode", rs.getString("zipcode"));
				user.put("phone", rs.getString("phone"));
				user.put("mobile", rs.getString("mobile"));
				user.put("branchcode", rs.getString("branchcode"));
				user.put("categorycode", rs.getString("categorycode"));
				user.put("borrowernotes", rs.getString("borrowernotes"));
				user.put("dateenrolled", rs.getString("dateenrolled"));
				user.put("dateexpiry", rs.getString("dateexpiry"));
				
				
			}
			
		} catch(SQLException ex){
			log.error("No se pudo consulta la información de usuario", ex);
		}
		
		return user;
		
	}
	
	
	
	/**
	 * Buscar direcciones de contacto por Usuario
	 * @param id
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 */
	public UserAddressInformation lookupAddressesByUser(String id){
		UserAddressInformation address = new UserAddressInformation();
		
		
		try{
			Map<String, String> user = lookupUserInfo(id);
			PhysicalAddress physical_address = new PhysicalAddress();
			
			if(user.get("address") != null || user.get("address") != ""){
				UnstructuredAddress unstructured_address = new UnstructuredAddress();
				unstructured_address.setUnstructuredAddressData(user.get("address"));
				
				physical_address.setUnstructuredAddress(unstructured_address);
				
				address.setPhysicalAddress(physical_address);
				
			}
			
			if(user.get("email") != null || user.get("email") != ""){
				ElectronicAddress email_address = new ElectronicAddress();
				email_address.setElectronicAddressData(user.get("email"));
				
				
				address.setElectronicAddress(email_address);
			}
			
			
			
			
		}
		catch(NullPointerException npe){
			log.error("No se pudo consultar las direcciones del usuario",npe);
		}
		
		return address;
		
	}
	
	/**
	 * Buscar Información de nombre por Usuario
	 * @param id
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 */
	public NameInformation lookupNameInformationByUser(String id){
		NameInformation name = new NameInformation();
		
		
		try{
			Map<String, String> user = lookupUserInfo(id);
			
			
			if(user.get("fullname") != null || user.get("fullname") != ""){
				PersonalNameInformation p = new PersonalNameInformation();
				p.setUnstructuredPersonalUserName(user.get("fullname"));
				
				name.setPersonalNameInformation(p);
				
			}
			
			
			
		}
		catch(NullPointerException npe){
			log.error("No se pudo consultar las direcciones del usuario",npe);
		}
		
		return name;
	}
	
	/**
	 * Buscar Prestamos por Usuario
	 * @param id
	 * @param agencyId
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 * 
	 */
	public List<LoanedItem> lookupLoanedItemsByUser(String id, String agencyId){
		
		List<LoanedItem> loaned_items = new ArrayList<LoanedItem>();
		
		try{
			Connection con = openDbConnection();
			PreparedStatement pst = con.prepareStatement(
					"select "
					+ "is.*, "
					+ "bi.title, ExtractValue(bi.marcxml,'//controlfield[@tag=\"001\"]') as cn "
					+ "from issues is "
					+ "join borrowers bo on is.borrowernumber=bo.borrowernumber and bo.cardnumber = ?"
					+ "join biblio bi on bi.biblionumber=bo.biblionumber"
					);
			pst.setString(1,(String) id);
			ResultSet rs = pst.executeQuery();
			
			while(rs.next()){
				LoanedItem item = new LoanedItem();
								
				Timestamp dueDate = rs.getTimestamp("expirationdate");
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(dueDate);
				item.setDateDue(gc);
				item.setReminderLevel(new BigDecimal(1));
				
				ItemId itemId = new ItemId();
				itemId.setAgencyId(new AgencyId(agencyId));
				itemId.setItemIdentifierValue(rs.getString("barcode"));
				itemId.setItemIdentifierType(Version1ItemIdentifierType.BARCODE);
				item.setItemId(itemId);
				
				item.setTitle(rs.getString("title"));
				
				BibliographicDescription bibliographicDescription = new BibliographicDescription();
				List<BibliographicRecordId> bibliographicRecordIds = new ArrayList<BibliographicRecordId>();
				BibliographicRecordId bibRecordId = new BibliographicRecordId();
				bibRecordId.setBibliographicRecordIdentifier(rs.getString("cn"));
				bibRecordId.setAgencyId(new AgencyId(agencyId));	
				bibliographicRecordIds.add(bibRecordId);
				bibliographicDescription.setBibliographicRecordIds(bibliographicRecordIds);
				item.setBibliographicDescription(bibliographicDescription);
				
				/*
				Amount amount = new Amount();
				amount.setCurrencyCode(new CurrencyCode("COP", 1000));
				amount.setMonetaryValue(new BigDecimal(0));
				item.setAmount(amount);
				*/
				
				loaned_items.add(item);
				
			}
			
			
			
			
		} catch(SQLException ex){
			log.error("No se pudo consultar los bloqueos del usuario", ex);
		}
		
		return loaned_items;
		
	}
	
	/**
	 * Buscar Solicitudes de Prestamos por Usuario
	 * @param id
	 * @param agencyId
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 * 
	 */
	
	public List<RequestedItem> lookupRequestedItemsByUser(String id, String agencyId){
		
		List<RequestedItem> requested_items = new ArrayList<RequestedItem>();
		
		try{
			Connection con = openDbConnection();
			PreparedStatement pst = con.prepareStatement("select r.*,"
					+ "i.barcode, "
					+ "ExtractValue(bi.marcxml,'//controlfield[@tag=\"001\"]') as cn"
					+ "from reserves r "
					+ "inner join items i on r.itemnumber=i.itemnumber "
					+ "inner join biblio b  where r.borrowernumber = ? and r.branchcode = ?"
					+ "inner join biblioitems bi where b.biblionumber = bi.biblionumber");
			pst.setString(1,(String) id);
			pst.setString(2,(String) agencyId);
			ResultSet rs = pst.executeQuery();
			
			while(rs.next()){
				RequestedItem item = new RequestedItem();
				
				Timestamp expireDate = rs.getTimestamp("expirationdate");
				GregorianCalendar edgc = new GregorianCalendar();
				edgc.setTime(expireDate);
				item.setPickupExpiryDate(edgc);
				item.setReminderLevel(new BigDecimal(1));
				
				Timestamp pickupDate = rs.getTimestamp("reservedate");
				GregorianCalendar pgc = new GregorianCalendar();
				pgc.setTime(pickupDate);
				item.setPickupDate(pgc);
				
				Timestamp datePlaced = rs.getTimestamp("waitingdate");
				GregorianCalendar dpgc = new GregorianCalendar();
				if(datePlaced!=null){
					dpgc.setTime(datePlaced);
					item.setDatePlaced(dpgc);
				}
				else{
					item.setDatePlaced(dpgc);
				}
				
				
				
				ItemId itemId = new ItemId();
				itemId.setAgencyId(new AgencyId(agencyId));
				itemId.setItemIdentifierValue(rs.getString("barcode"));
				itemId.setItemIdentifierType(Version1ItemIdentifierType.BARCODE);
				item.setItemId(itemId);
				
				item.setTitle(rs.getString("title"));
				
				BibliographicDescription bibliographicDescription = new BibliographicDescription();
				List<BibliographicRecordId> bibliographicRecordIds = new ArrayList<BibliographicRecordId>();
				BibliographicRecordId bibRecordId = new BibliographicRecordId();
				bibRecordId.setBibliographicRecordIdentifier(rs.getString("cn"));
				bibRecordId.setBibliographicRecordIdentifierCode(Version1BibliographicRecordIdentifierCode.CN);
				bibRecordId.setAgencyId(new AgencyId(agencyId));	
				bibliographicRecordIds.add(bibRecordId);
				bibliographicDescription.setBibliographicRecordIds(bibliographicRecordIds);
				item.setBibliographicDescription(bibliographicDescription);
				
				item.setRequestType(XcRequestType.HOLD);
				item.setRequestStatusType(new RequestStatusType(rs.getString("found")));
				
				requested_items.add(item);
				
			}
			
			return requested_items;
			
			
		} catch(SQLException ex){
			log.error("No se pudo consultar las peticiones de material del usuario", ex);
		} catch(NullPointerException npe){
			log.error("No se pudo consultar las peticiones de material del usuario", npe);
			return requested_items;
		}
		
		return null;
		
	}
	
	
	/**
	 * Buscar descripción bibliográfica por id bibliográfico
	 * @param bibId
	 * @return 
	 * @author pabloadi
	 * @throws ILSException
	 */
	
	public BibliographicDescription lookupBibliographicDescriptionByBibId(BibliographicId bibId) throws ILSException {
		
		BibliographicDescription bibDescription = new BibliographicDescription();
		PreparedStatement pst;
		ResultSet rs = null;
		
		try{
			Connection con = openDbConnection();
						
			if(bibId.getBibliographicItemId().getBibliographicItemIdentifier() != null){
				
				if(bibId.getBibliographicItemId().getBibliographicItemIdentifierCode().matches(Version1BibliographicItemIdentifierCode.ISBN)){
					pst = con.prepareStatement("select b.*,bi.* from biblio b natural join biblioitems bi where isbn = ? limit 1");
					pst.setString(1,(String) bibId.getBibliographicItemId().getBibliographicItemIdentifier());
					
					rs = pst.executeQuery();
				}
				
				if(bibId.getBibliographicItemId().getBibliographicItemIdentifierCode().matches(Version1BibliographicItemIdentifierCode.ISSN)){
					pst = con.prepareStatement("select b.*,bi.* from biblio b natural join biblioitems bi where issn = ? limit 1");
					pst.setString(1,(String) bibId.getBibliographicItemId().getBibliographicItemIdentifier());
					
					rs = pst.executeQuery();
				}
				
			}
			else if(bibId.getBibliographicRecordId().getBibliographicRecordIdentifier() != null){
				
				if(bibId.getBibliographicRecordId().getBibliographicRecordIdentifierCode().matches(Version1BibliographicRecordIdentifierCode.CN)){
					pst = con.prepareStatement("select b.*,bi.*  from biblio b natural join biblioitems bi j where ExtractValue(bi.marcxml,'//controlfield[@tag=\"001\"]') = ? and homebranch = ? limit 1");
					pst.setString(1,(String) bibId.getBibliographicRecordId().getBibliographicRecordIdentifier());
					pst.setString(2,(String) bibId.getBibliographicRecordId().getAgencyId().getValue());
					
					rs = pst.executeQuery();
				}
			}
						
			while(rs.next()){
				bibDescription.setAuthor(rs.getString("author"));
				bibDescription.setTitle(rs.getString("title"));
				bibDescription.setPublicationDate(rs.getString("publicationyear"));
				bibDescription.setPlaceOfPublication(rs.getString("place"));
				//TODO No está claro la forma de establecer el lenguaje
				//bibDescription.setLanguage(Version1Language.find(Version1Language.VERSION_1_LANGUAGE, rs.getString("idiomapub")));
				bibDescription.setMediumType(getMediumTypeFromILS(rs.getString("itype")));
				bibDescription.setPublisher(rs.getString("publishercode"));
				
				
			}
		}
		catch(SQLException ex){
			log.error("Hubo un error en la consulta de fichas", ex);
			throw new ILSException(ex.getMessage(), ex.getCause());
		}/*
		catch(ServiceException ex){
			log.error("Hubo un error en la selección del lenguaje para la ficha", ex);
			throw new ILSException(ex.getMessage(), ex.getCause());
		}
		*/
		return bibDescription;
	}
	
	
	/**
	 * Buscar descripción bibliográfica por id del material
	 * @param bibId
	 * @return 
	 * @author pabloadi
	 * @throws ILSException
	 */
	
	public BibliographicDescription lookupBibliographicDescriptionByItemId(ItemId itemId) throws ILSException {
		
		BibliographicDescription bibDescription = new BibliographicDescription();
		PreparedStatement pst;
		ResultSet rs;
				
		try{
			Connection con = openDbConnection();
			
			
			if(itemId.getItemIdentifierValue() != null && itemId.getItemIdentifierType().matches(Version1ItemIdentifierType.BARCODE)){
				
				
				
				pst = con.prepareStatement("select b.*,bi.*,ExtractValue(marcxml,'//controlfield[@tag=\"001\"]') as cn  from biblio b natural join biblioitems bi natural join items where barcode = ? and homebranch = ?");
				pst.setString(1,(String) itemId.getItemIdentifierValue());
				pst.setString(1,(String) itemId.getAgencyId().getValue());
				
				rs = pst.executeQuery();
				
				while(rs.next()){
					bibDescription.setAuthor(rs.getString("author"));
					bibDescription.setTitle(rs.getString("title"));
					bibDescription.setPublicationDate(rs.getString("publicationyear"));
					bibDescription.setPlaceOfPublication(rs.getString("place"));
					//TODO No está claro la forma de establecer el lenguaje
					//bibDescription.setLanguage(Version1Language.find(Version1Language.VERSION_1_LANGUAGE, rs.getString("idiomapub")));
					bibDescription.setMediumType(getMediumTypeFromILS(rs.getString("itype")));
					bibDescription.setPublisher(rs.getString("publishercode"));
					
					
					
					List<BibliographicRecordId> bibliographicRecordIds = new ArrayList<BibliographicRecordId>();
					BibliographicRecordId bibRecordId = new BibliographicRecordId();
					bibRecordId.setBibliographicRecordIdentifier(rs.getString("cn"));
					bibRecordId.setBibliographicRecordIdentifierCode(Version1BibliographicRecordIdentifierCode.CN);
					bibliographicRecordIds.add(bibRecordId);
					bibDescription.setBibliographicRecordIds(bibliographicRecordIds);
					
					
				}
			}
			
		}
		catch(SQLException ex){
			log.error("Hubo un error en la consulta de fichas", ex);
			throw new ILSException(ex.getMessage(), ex.getCause());
		}/*
		catch(ServiceException ex){
			throw new ILSException(ex.getMessage(),ex.getCause());
		}
		*/
		
		
		return bibDescription;
	}
	
	private Version1MediumType getMediumTypeFromILS(String type){
		if(type.equals("Book") || type.equals("Libro")){
			return Version1MediumType.BOOK;
		}
		else if(type.equals("Book With Audio Tape") || type.equals("Libro con audiocassete")){
			return Version1MediumType.BOOK_WITH_AUDIO_TAPE;
		}
		else if(type.equals("Book With Compact Disc") || type.equals("Libro con CD")){
			return Version1MediumType.BOOK_WITH_COMPACT_DISC;
		}
		else if(type.equals("Book With Diskette") || type.equals("Libro con Diskette")){
			return Version1MediumType.BOOK_WITH_DISKETTE;
		}
		else if(type.equals("Audio Tape") || type.equals("Audiocassete")){
			return Version1MediumType.AUDIO_TAPE;
		}
		else if(type.equals("CD") || type.equals("CD")){
			return Version1MediumType.CD_ROM;
		}
		else if(type.equals("Diskette") || type.equals("Diskette")){
			return Version1MediumType.DISKETTE;
		}
		else if(type.equals("Magazine") || type.equals("Revista")){
			return Version1MediumType.MAGAZINE;
		}
		else if(type.equals("Microform") || type.equals("Microfilme")){
			return Version1MediumType.MICROFORM;
		}
		else if(type.equals("Video Tape") || type.equals("Videocassete")){
			return Version1MediumType.VIDEO_TAPE;
		}
		
		return null;
	}
	
	/**
	 * Buscar HoldingSet por id bibliográfico
	 * @param bibId
	 * @param initData
	 * @return
	 * @author pabloadi
	 * @throws ILSException 
	 */
	
	public HoldingsSet lookupHoldingsSetByBidId(BibliographicId bibId, LookupItemSetInitiationData initData) throws ILSException {
		
		HoldingsSet holdingSet = new HoldingsSet();
		List<ItemInformation> itemInformations = new ArrayList<ItemInformation>();
		
		try{
			Connection con = openDbConnection();
			PreparedStatement pst = null;
			
			if(bibId.getBibliographicItemId().getBibliographicItemIdentifier() != null){
				
				if(bibId.getBibliographicItemId().getBibliographicItemIdentifierCode().matches(Version1BibliographicItemIdentifierCode.ISBN)){
					pst = con.prepareStatement("select i.* from items i natural join biblioitems bi where bi.isbn = ?");
					pst.setString(1,(String) bibId.getBibliographicItemId().getBibliographicItemIdentifier());
					
				}
				else if(bibId.getBibliographicItemId().getBibliographicItemIdentifierCode().matches(Version1BibliographicItemIdentifierCode.ISSN)){
					pst = con.prepareStatement("select i.* from items i natural join biblioitems bi where bi.issn = ?");
					pst.setString(1,(String) bibId.getBibliographicItemId().getBibliographicItemIdentifier());
					
				}
				
			}
			else if(bibId.getBibliographicRecordId().getBibliographicRecordIdentifier() != null){
				
				if(bibId.getBibliographicRecordId().getBibliographicRecordIdentifierCode().matches(Version1BibliographicRecordIdentifierCode.CN)){
					pst = con.prepareStatement("select i.* from items i natural join biblioitems bi where ExtractValue(bi.marcxml,'//controlfield[@tag=\"001\"]') = ?");
					pst.setString(1,(String) bibId.getBibliographicRecordId().getBibliographicRecordIdentifier());
				}
			}
			
			ResultSet rs = pst.executeQuery();
			
			while(rs.next()){
				
				ItemInformation itemInfo = new ItemInformation();
				
				ItemId itemId = new ItemId();
				AgencyId agencyId = new AgencyId(rs.getString("homebranch"));
				itemId.setAgencyId(agencyId);
				itemId.setItemIdentifierValue(rs.getString("barcode"));
				itemId.setItemIdentifierType(Version1ItemIdentifierType.BARCODE);
				ItemOptionalFields itemOptionals = new ItemOptionalFields();
				
				if(initData.getCirculationStatusDesired()){
					
					itemOptionals.setCirculationStatus(checkCirculationStatusByItemId(itemId,agencyId)); 
				}
				
				if(initData.getCurrentBorrowerDesired()){
					try{
						itemInfo.setCurrentBorrower(checkCurrentBorrowerByItemId(itemId,agencyId));
					}
					catch(ILSException ex){
						
						throw new ILSException(ex.getMessage(), ex.getCause());
					}
				}
				
				if(initData.getCurrentRequestersDesired()){
					
				}
				
				if(initData.getElectronicResourceDesired()){
					
				}
				
				if(initData.getHoldQueueLengthDesired()){
					
				}
				
				if(initData.getItemDescriptionDesired()){
					
				}
				
				if(initData.getItemUseRestrictionTypeDesired()){
					
				}
				
				if(initData.getLocationDesired()){
					
				}
				
				if(initData.getPhysicalConditionDesired()){
					
				}
				
				if(initData.getSecurityMarkerDesired()){
					
				}
				
				if(initData.getBibliographicDescriptionDesired()){
					
					itemOptionals.setBibliographicDescription(lookupBibliographicDescriptionByBibId(bibId));
					
					
				}
				
				itemInfo.setItemOptionalFields(itemOptionals);
				itemInformations.add(itemInfo);
				
			}
			
			holdingSet.setItemInformations(itemInformations);
			
		}
		catch(SQLException ex){
			throw new ILSException(ex.getMessage(), ex.getCause());
		}
		
		
		return holdingSet;
	}
	
	/**
	 * Verificar estatus de circulación por id de item
	 * @param itemId
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 */
	
	public Version1CirculationStatus checkCirculationStatusByItemId(ItemId itemId, AgencyId agencyId) throws ILSException{
		
		Connection con;
		Version1CirculationStatus circulation_status = Version1CirculationStatus.AVAILABLE_ON_SHELF;
		
		
		try{
		    con = openDbConnection();
			PreparedStatement pst = con.prepareStatement("select r.reserve_id from reserves r inner join items i on r.biblionumber = i.biblionumber where i.barcode = ?");
			pst.setString(1,(String) itemId.getItemIdentifierValue());
			pst.setString(2,(String) agencyId.getValue());
			ResultSet rs = pst.executeQuery();
			
			if(rs.first()){
				circulation_status = Version1CirculationStatus.ON_LOAN;
			}
		}
		catch(SQLException ex){
			throw new ILSException(ex.getMessage(), ex.getCause());
		}
		
		
		try{
		    con = openDbConnection();
			PreparedStatement pst = con.prepareStatement("select is.issue_id from issues is inner join items i on is.biblionumber = i.biblionumber where i.barcode = ?");
			pst.setString(1,(String) itemId.getItemIdentifierValue());
			pst.setString(2,(String) agencyId.getValue());
			ResultSet rs = pst.executeQuery();
			
			if(rs.first()){
				circulation_status = Version1CirculationStatus.AVAILABLE_FOR_PICKUP;
			}
		}
		catch(SQLException ex){
			throw new ILSException(ex.getMessage(), ex.getCause());
		}
		
		
		//TODO Establecer mas estados de circulación propuestos por KOHA
		
		return circulation_status;
	}
	
	
	/**
	 * Verificar usuario prestante del elemento por id de item
	 * @param itemId
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 */
	
	public CurrentBorrower checkCurrentBorrowerByItemId(ItemId itemId, AgencyId agencyId) throws ILSException {
		
		Connection con;
		CurrentBorrower current_borrower = null;
		
		
		try{
		    con = openDbConnection();
			PreparedStatement pst = con.prepareStatement("select b.* from borrowers b join items i on where i.barcode = ? and i.homebranch = ?");
			pst.setString(1,(String) itemId.getItemIdentifierValue());
			pst.setString(2,(String) agencyId.getValue());
			ResultSet rs = pst.executeQuery();
			
			if(rs.first()){
				current_borrower = new CurrentBorrower();
				UserId userId = new UserId();
				userId.setUserIdentifierValue(rs.getString("cardnumber"));
				userId.setUserIdentifierType(Version1UserIdentifierType.INSTITUTION_ID_NUMBER);
				userId.setAgencyId(agencyId);
				current_borrower.setUserId(userId);
			}
		}
		catch(SQLException ex){
			log.error("Hubo un error en la consulta del usuario prestante", ex);
			throw new ILSException(ex.getMessage(),ex.getCause());
		}
		
		return current_borrower;
		
	}
	
	public boolean registerUser(UserOptionalFields userInfo) throws ILSException {
		
		Connection con;
		PreparedStatement pst = null;
		java.util.Calendar fin_vigencia = Calendar.getInstance();
		fin_vigencia.add(Calendar.YEAR, 1);
		
		try{
			con = openDbConnection();
			pst = con.prepareStatement("insert into borrowers " +
					"("
					+ "cardnumber,"
					+ "surname,"
					+ "firstname,"
					+ "address,"
					+ "email,"
					+ "contactnote,"
					+ "branchcode,"
					+ "categorycode,"
					+ "dateenrolled,"
					+ "dateexpiry) " +
					"values (?,?,?,?,?,?,?,?,?,?,?)");
			pst.setString(0, userInfo.getUserId(0).getUserIdentifierValue());
			pst.setString(1, userInfo.getNameInformation().getPersonalNameInformation().getStructuredPersonalUserName().getSurname());
			pst.setString(2, userInfo.getNameInformation().getPersonalNameInformation().getStructuredPersonalUserName().getGivenName());
			pst.setString(3, userInfo.getUserAddressInformation(0).getPhysicalAddress().getUnstructuredAddress().getUnstructuredAddressData());
			pst.setString(4, userInfo.getUserAddressInformation(0).getElectronicAddress().getElectronicAddressData());
			pst.setString(5, "usuario creado por el servicio de NCIP de la plataforma BABEL");
			pst.setString(6, (String) Constants.CONFIG_ILS_DEFAULT_AGENCY);
			pst.setString(7, "IL"); //Usuario de préstamo interbibliotecario
			pst.setDate(8, new java.sql.Date(Calendar.getInstance().getTime().getTime()));
			pst.setDate(9, new java.sql.Date(fin_vigencia.getTime().getTime()));
						
			pst.executeUpdate();
			
		}
		catch(SQLException ex){
			log.error("Hubo un error en la registro del usuario", ex);
			throw new ILSException(ex.getMessage(), ex.getCause());
			
		}
		
		return true;
	}
	
	
	/**
	 * Realizar solicitud de prestamo de un elemento por id de item
	 * @param initData
	 * @return
	 * @author pabloadi
	 * @throws ILSException
	 * @throws SQLException 
	 */
	
	public void requestItems(RequestItemInitiationData initData) throws ILSException, NoUserFoundException, ItemNotAvailableException, NoItemFoundException, SQLException {
		
		
		Map<String, String> user = lookupUserInfo(initData.getUserId().getUserIdentifierValue());
		UserId userId = null;
		
		
		if(user != null){
			userId = new UserId();
			userId.setAgencyId(new AgencyId((String) Constants.CONFIG_ILS_DEFAULT_AGENCY));
			userId.setUserIdentifierValue(user.get("cardnumber"));
			userId.setUserIdentifierType(Version1UserIdentifierType.INSTITUTION_ID_NUMBER);
			
		}
		else{
			log.info("No existe el usuario en la base de datos de KOHA, remitiendo al servicio de creación de usuario ...");
			throw new NoUserFoundException("No existe el usuario en la base de datos de KOHA, por favor solicite la creación del usuario por medio de NCIP ...");
		}
				
		for(ItemId itemId : initData.getItemIds()){
			
		
			// verificar el estado de circulación del item
			Version1CirculationStatus circulation_status = checkCirculationStatusByItemId(itemId, itemId.getAgencyId());
			BibliographicDescription bib_description = lookupBibliographicDescriptionByItemId(itemId); 
			
			if(circulation_status != Version1CirculationStatus.AVAILABLE_ON_SHELF){
				log.info("El material no está disponible para reserva");
				throw new ItemNotAvailableException(circulation_status.toString());
			}
			
			if(bib_description == null){
				log.info("No existe descripción bibliográfica del material. No se puede reservar el material");
				throw new NoItemFoundException("No existe descripción del material, se puede decir que el material no existe");
			}
			
						
			Connection con = null;
			PreparedStatement pst = null;
			
			try{
			    con = openDbConnection();
				pst = con.prepareStatement("insert into reserves " +
						"("
						+ "branchcode, "
						+ "itemnumber, "
						+ "borrowernumber, "
						+ "expirationdate, "
						+ "reservenotes)" +
						"values " +
						"("
						+ "?,"
						+ "(select itemnumber from items where barcode = ? limit 1),"
						+ "(select borrowernumber from borrowers where cardnumber = ? limit 1),"
						+ "?,"
						+ "?,"
						+ "?)");
				pst.setString(1,(String) Constants.CONFIG_ILS_DEFAULT_AGENCY);
				pst.setString(2,(String) itemId.getItemIdentifierValue());
				pst.setString(3,(String) userId.getUserIdentifierValue());
				pst.setDate(4, new java.sql.Date(initData.getPickupExpiryDate().getTimeInMillis()));
				pst.setString(5, "Préstamo interbibliotecario: El usuario es: \n"
						+ userId.getUserIdentifierValue()
						+ "\n de la biblioteca o institución universitaria: \n"
						+ userId.getAgencyId().getValue()
						+ "\n desea solicitar el siguiente material: \n"
						+ itemId.getItemIdentifierValue()
						+ "con tipo de identificación de: \n"
						+ itemId.getItemIdentifierType()
						+ " de la biblioteca: \n"
						+ itemId.getAgencyId().getValue());
				
				
				pst.executeUpdate();
				

			}
			catch(SQLException ex){
				log.error("Hubo un error en el registro de la solicitud de préstamo", ex);
				throw new ILSException(ex.getMessage());
			}
			
		}
		
	}
	
	
	
	
	
}

