/**
 * Copyright (c) 2010 eXtensible Catalog Organization
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the MIT/X11 license. The text of the license can be
 * found at http://www.opensource.org/licenses/mit-license.php.
 */

package org.extensiblecatalog.ncip.v2.koha;



import java.util.ArrayList;
import java.util.List;


import org.apache.log4j.Logger;
import org.extensiblecatalog.ncip.v2.service.*;

/**
 * This class implements Lookup Item service for siabuc9 connector
 * @author pabloadi
 *
 */
public class KohaLookupItemSetService implements LookupItemSetService {
	
	static Logger log = Logger.getLogger(KohaLookupItemSetService.class);
	KohaRemoteServiceManager siabucSvcMgr;
	
	@Override
	public LookupItemSetResponseData performService(
			LookupItemSetInitiationData initData,
			ServiceContext serviceContext, RemoteServiceManager serviceManager)
			throws ServiceException {
		// TODO Auto-generated method stub
		
		siabucSvcMgr = (KohaRemoteServiceManager) serviceManager;
		LookupItemSetResponseData lookupItemSetResponseData = new LookupItemSetResponseData();
		
		List<BibliographicId> bibliographicIds = initData.getBibliographicIds();
		// TODO Agregar listado de problemas
		//List<Problem> problems = new ArrayList<Problem>();
		
		if(bibliographicIds != null && bibliographicIds.size()>0){
			ArrayList<BibInformation> bibInformations = new ArrayList<BibInformation>();
			
			for(BibliographicId bibId : bibliographicIds){
				BibInformation bibInfo = new BibInformation();
				List<HoldingsSet> holdingsSets = new ArrayList<HoldingsSet>();
				try{
					holdingsSets.add(siabucSvcMgr.lookupHoldingsSetByBidId(bibId, initData));
				}
				catch(Exception ex){
					
				}
								
				bibInfo.setBibliographicId(bibId);
				bibInfo.setHoldingsSets(holdingsSets);
				
				
			}
			
			
			
			lookupItemSetResponseData.setBibInformations(bibInformations);
		}
		else{
			
		}
		
		return lookupItemSetResponseData;
	}
		
	
	

	
}
