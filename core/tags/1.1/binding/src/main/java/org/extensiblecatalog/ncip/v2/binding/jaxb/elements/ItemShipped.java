//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-661 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.04.27 at 07:18:33 PM COT 
//


package org.extensiblecatalog.ncip.v2.binding.jaxb.elements;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.niso.org/2008/ncip}InitiationHeader" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element ref="{http://www.niso.org/2008/ncip}ItemId"/>
 *           &lt;sequence>
 *             &lt;element ref="{http://www.niso.org/2008/ncip}RequestId"/>
 *             &lt;element ref="{http://www.niso.org/2008/ncip}ItemId" minOccurs="0"/>
 *           &lt;/sequence>
 *         &lt;/choice>
 *         &lt;element ref="{http://www.niso.org/2008/ncip}UserId" minOccurs="0"/>
 *         &lt;element ref="{http://www.niso.org/2008/ncip}DateShipped"/>
 *         &lt;element ref="{http://www.niso.org/2008/ncip}ShippingInformation"/>
 *         &lt;element ref="{http://www.niso.org/2008/ncip}ItemOptionalFields" minOccurs="0"/>
 *         &lt;element ref="{http://www.niso.org/2008/ncip}UserOptionalFields" minOccurs="0"/>
 *         &lt;element ref="{http://www.niso.org/2008/ncip}Ext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "content"
})
@XmlRootElement(name = "ItemShipped")
public class ItemShipped {

    @XmlElementRefs({
        @XmlElementRef(name = "ShippingInformation", namespace = "http://www.niso.org/2008/ncip", type = ShippingInformation.class),
        @XmlElementRef(name = "ItemOptionalFields", namespace = "http://www.niso.org/2008/ncip", type = ItemOptionalFields.class),
        @XmlElementRef(name = "Ext", namespace = "http://www.niso.org/2008/ncip", type = Ext.class),
        @XmlElementRef(name = "DateShipped", namespace = "http://www.niso.org/2008/ncip", type = JAXBElement.class),
        @XmlElementRef(name = "InitiationHeader", namespace = "http://www.niso.org/2008/ncip", type = InitiationHeader.class),
        @XmlElementRef(name = "UserOptionalFields", namespace = "http://www.niso.org/2008/ncip", type = UserOptionalFields.class),
        @XmlElementRef(name = "RequestId", namespace = "http://www.niso.org/2008/ncip", type = RequestId.class),
        @XmlElementRef(name = "UserId", namespace = "http://www.niso.org/2008/ncip", type = UserId.class),
        @XmlElementRef(name = "ItemId", namespace = "http://www.niso.org/2008/ncip", type = ItemId.class)
    })
    protected List<Object> content;

    /**
     * Gets the rest of the content model. 
     * 
     * <p>
     * You are getting this "catch-all" property because of the following reason: 
     * The field name "ItemId" is used by two different parts of a schema. See: 
     * line 1146 of file:/media/pabloadi/datos/proyectos/tesis/xcnciptoolkit/xcncip2toolkit/core/tags/1.1/binding/src/main/xsd/ncip_v2_01.xsd
     * line 1143 of file:/media/pabloadi/datos/proyectos/tesis/xcnciptoolkit/xcncip2toolkit/core/tags/1.1/binding/src/main/xsd/ncip_v2_01.xsd
     * <p>
     * To get rid of this property, apply a property customization to one 
     * of both of the following declarations to change their names: 
     * Gets the value of the content property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemOptionalFields }
     * {@link ShippingInformation }
     * {@link Ext }
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link InitiationHeader }
     * {@link UserOptionalFields }
     * {@link RequestId }
     * {@link UserId }
     * {@link ItemId }
     * 
     * 
     */
    public List<Object> getContent() {
        if (content == null) {
            content = new ArrayList<Object>();
        }
        return this.content;
    }

}
